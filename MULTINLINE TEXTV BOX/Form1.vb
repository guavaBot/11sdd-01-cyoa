﻿Imports System.IO

Public Class Form1
    Dim filepath As String = ""
    Dim arrRooms(25) As room
    Dim isIt As Integer
    Dim oFlloo As String
    Dim gender As String

    Structure room
        Dim id As Integer
        Dim title As String
        Dim story As String
        Dim ch1Txt As String
        Dim ch1ID As Integer
        Dim ch2Txt As String
        Dim ch2ID As Integer
        Dim ch3Txt As String
        Dim ch3ID As Integer
    End Structure

    Private Sub loadfile()
        'File format is:
        ' ID | Title | Story | ChoiceID1| Choice 1 | ChoiceID2 | Choice 2 | ChoiceID3 | Choice3
        Dim objReader As New System.IO.StreamReader(filepath)
        Dim theText As String = ""
        Dim tmp
        Dim i As Integer

        While objReader.Peek() <> -1
            theText = objReader.ReadLine()
            tmp = Split(theText, "|")
            arrRooms(i).id = Int(tmp(0))
            arrRooms(i).title = tmp(1)
            arrRooms(i).story = tmp(2)
            arrRooms(i).ch1ID = Int(tmp(3))
            arrRooms(i).ch1Txt = tmp(4)
            arrRooms(i).ch2ID = Int(tmp(5))
            arrRooms(i).ch2Txt = tmp(6)
            arrRooms(i).ch3ID = Int(tmp(7))
            arrRooms(i).ch3Txt = tmp(8)
            i += 1
        End While

    End Sub



    Private Sub Form1_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        txtStory.Visible = False
        PictureBox1.Visible = False
        Button1.Visible = False
        Button2.Visible = False
        Button3.Visible = False

        isIt = 1
        Dim Fun As New System.IO.StreamReader(Application.StartupPath & "\comp.txt")
        Dim quest As String = ""

        While Fun.Peek() <> -1
            quest = Fun.ReadLine()
        End While
        init.Text = quest.Replace("**", vbNewLine)

        If System.IO.File.Exists(Application.StartupPath & "\text.txt") Then
            filepath = Application.StartupPath & "\text.txt"
        Else
            MsgBox("No game file ofund!", MsgBoxStyle.Critical, "Error")
            End
        End If


    End Sub

    Private Sub loadRoom(roomID As Integer)
        txtStory.Text = arrRooms(roomID).story.Replace("**", vbNewLine).Replace("name", oFlloo).Replace("gender", gender)
        Button1.Text = arrRooms(roomID).ch1Txt
        Button2.Text = arrRooms(roomID).ch2Txt
        Button3.Text = arrRooms(roomID).ch3Txt
        Button1.Tag = arrRooms(roomID).ch1ID
        Button2.Tag = arrRooms(roomID).ch2ID
        Button3.Tag = arrRooms(roomID).ch3ID

    End Sub

    Public Sub Enter_Click(sender As Object, e As EventArgs) Handles Enter.Click
        If isIt = 1 Then
            oFlloo = Answer.Text

            Question.Text = "What is your gender? Male, Female, Both, or Neither?"
            Question.Height += 14
            Answer.Enabled = False
            Answer.Visible = False
            Answer2.Visible = True
            Answer2.Enabled = True
            isIt = 2
        ElseIf isIt = 2 Then
            gender = Me.Answer2.Text
            gender = gender.ToLower()
            Question.Visible = False
            Answer2.Visible = False
            Answer2.Enabled = False
            init.Visible = False
            isIt = 3
            Enter.Text = "Begin"
        Else
            Enter.Visible = False
            PictureBox2.Visible = False
            txtStory.Visible = True
            PictureBox1.Visible = True
            PictureBox3.Visible = True
            Button1.Visible = True
            Button2.Visible = True
            Button3.Visible = True
            TextBox1.Visible = True
            TextBox2.Visible = True
            Spook.Visible = True
            Bore.Visible = True
            Form2.Show()
            Me.Hide()
            loadfile()
            loadRoom(0)
        End If
    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        deathMsg.Visible = False
        If Bore.Value < 4 Then
            Spook.Value = 100
            Bore.Value = 0
        ElseIf sender.tag = 21 Then
            Spook.Value = 50
            Bore.Value = 50
        ElseIf sender.tag = 13 Or sender.tag = 14 Then
            deathMsg.Visible = True
            If Spook.Value > Bore.Value Then
                Dim spoook As String
                spoook = Spook.Value
                deathMsg.Text = "You were " + spoook + " percent ethically bad with your choices"
            ElseIf Spook.Value < Bore.Value Then
                Dim booore As String
                booore = Bore.Value
                deathMsg.Text = "You were " + booore + " percent ethically good with your choices"
            Else
                deathMsg.Text = "You were ethically neutral with your choices"
            End If
        Else
            Spook.Value += 3
            Bore.Value -= 3

        End If
        loadRoom(sender.tag)

    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        deathMsg.Visible = False
        If Spook.Value < 4 Then
            Spook.Value = 0
            Bore.Value = 100
        ElseIf sender.tag = 20 Then
            Spook.Value = 50
            Bore.Value = 50
        ElseIf sender.tag = 13 Or sender.tag = 14 Then
            deathMsg.Visible = True
            If Spook.Value > Bore.Value Then
                Dim spoook As String
                spoook = Spook.Value
                deathMsg.Text = "You were " + spoook + " percent ethically bad with your choices"
            ElseIf Spook.Value < Bore.Value Then
                Dim booore As String
                booore = Bore.Value
                deathMsg.Text = "You were " + booore + " percent ethically good with your choices"
            Else
                deathMsg.Text = "You were ethically neutral with your choices"
            End If
        Else
            Spook.Value -= 3
            Bore.Value += 3
        End If
        loadRoom(sender.tag)
    End Sub

    Private Sub Button3_Click(sender As Object, e As EventArgs) Handles Button3.Click
        deathMsg.Visible = False
        If sender.tag = 21 Then
            Spook.Value = 50
            Bore.Value = 50
            End
        ElseIf sender.tag = 13 Or sender.tag = 14 Then
            deathMsg.Visible = True
            If Spook.Value > Bore.Value Then
                Dim spoook As String
                spoook = Spook.Value
                deathMsg.Text = "You were " + spoook + " percent ethically bad with your choices"
            ElseIf Spook.Value < Bore.Value Then
                Dim booore As String
                booore = Bore.Value
                deathMsg.Text = "You were " + booore + " percent ethically good with your choices"
            Else
                deathMsg.Text = "You were ethically neutral with your choices"
            End If
        End If
        loadRoom(sender.tag)
    End Sub
End Class
