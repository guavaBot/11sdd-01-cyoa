﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.txtStory = New System.Windows.Forms.TextBox()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.Button3 = New System.Windows.Forms.Button()
        Me.init = New System.Windows.Forms.TextBox()
        Me.Question = New System.Windows.Forms.TextBox()
        Me.Answer = New System.Windows.Forms.TextBox()
        Me.Enter = New System.Windows.Forms.Button()
        Me.Answer2 = New System.Windows.Forms.TextBox()
        Me.PictureBox3 = New System.Windows.Forms.PictureBox()
        Me.PictureBox1 = New System.Windows.Forms.PictureBox()
        Me.PictureBox2 = New System.Windows.Forms.PictureBox()
        Me.Spook = New System.Windows.Forms.ProgressBar()
        Me.Bore = New System.Windows.Forms.ProgressBar()
        Me.TextBox1 = New System.Windows.Forms.TextBox()
        Me.TextBox2 = New System.Windows.Forms.TextBox()
        Me.deathMsg = New System.Windows.Forms.TextBox()
        CType(Me.PictureBox3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'txtStory
        '
        Me.txtStory.Enabled = False
        Me.txtStory.Font = New System.Drawing.Font("Consolas", 10.0!)
        Me.txtStory.Location = New System.Drawing.Point(1, 0)
        Me.txtStory.Multiline = True
        Me.txtStory.Name = "txtStory"
        Me.txtStory.Size = New System.Drawing.Size(534, 90)
        Me.txtStory.TabIndex = 0
        '
        'Button1
        '
        Me.Button1.BackColor = System.Drawing.SystemColors.ControlDark
        Me.Button1.Location = New System.Drawing.Point(8, 104)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(167, 86)
        Me.Button1.TabIndex = 2
        Me.Button1.Text = "Button1"
        Me.Button1.UseVisualStyleBackColor = False
        '
        'Button2
        '
        Me.Button2.BackColor = System.Drawing.SystemColors.ControlDark
        Me.Button2.Location = New System.Drawing.Point(8, 204)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(167, 86)
        Me.Button2.TabIndex = 3
        Me.Button2.Text = "Button2"
        Me.Button2.UseVisualStyleBackColor = False
        '
        'Button3
        '
        Me.Button3.BackColor = System.Drawing.SystemColors.ControlDark
        Me.Button3.Location = New System.Drawing.Point(8, 303)
        Me.Button3.Name = "Button3"
        Me.Button3.Size = New System.Drawing.Size(167, 86)
        Me.Button3.TabIndex = 4
        Me.Button3.Text = "Button3"
        Me.Button3.UseVisualStyleBackColor = False
        '
        'init
        '
        Me.init.Enabled = False
        Me.init.Font = New System.Drawing.Font("Courier New", 7.75!)
        Me.init.Location = New System.Drawing.Point(25, 12)
        Me.init.Multiline = True
        Me.init.Name = "init"
        Me.init.Size = New System.Drawing.Size(482, 325)
        Me.init.TabIndex = 5
        '
        'Question
        '
        Me.Question.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.Question.Enabled = False
        Me.Question.Font = New System.Drawing.Font("Courier New", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Question.Location = New System.Drawing.Point(134, 54)
        Me.Question.Multiline = True
        Me.Question.Name = "Question"
        Me.Question.Size = New System.Drawing.Size(237, 14)
        Me.Question.TabIndex = 7
        Me.Question.Text = "What is your name?"
        '
        'Answer
        '
        Me.Answer.BackColor = System.Drawing.SystemColors.Window
        Me.Answer.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.Answer.Location = New System.Drawing.Point(135, 74)
        Me.Answer.Multiline = True
        Me.Answer.Name = "Answer"
        Me.Answer.Size = New System.Drawing.Size(248, 90)
        Me.Answer.TabIndex = 8
        '
        'Enter
        '
        Me.Enter.BackColor = System.Drawing.SystemColors.Window
        Me.Enter.Location = New System.Drawing.Point(62, 345)
        Me.Enter.Name = "Enter"
        Me.Enter.Size = New System.Drawing.Size(408, 37)
        Me.Enter.TabIndex = 9
        Me.Enter.Text = "Enter"
        Me.Enter.UseVisualStyleBackColor = False
        '
        'Answer2
        '
        Me.Answer2.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.Answer2.Enabled = False
        Me.Answer2.Location = New System.Drawing.Point(134, 89)
        Me.Answer2.Multiline = True
        Me.Answer2.Name = "Answer2"
        Me.Answer2.Size = New System.Drawing.Size(237, 76)
        Me.Answer2.TabIndex = 10
        Me.Answer2.Visible = False
        '
        'PictureBox3
        '
        Me.PictureBox3.BackColor = System.Drawing.Color.Transparent
        Me.PictureBox3.Image = Global.MULTINLINE_TEXTV_BOX.My.Resources.Resources.Drip
        Me.PictureBox3.Location = New System.Drawing.Point(178, 95)
        Me.PictureBox3.Name = "PictureBox3"
        Me.PictureBox3.Size = New System.Drawing.Size(87, 76)
        Me.PictureBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox3.TabIndex = 11
        Me.PictureBox3.TabStop = False
        Me.PictureBox3.Visible = False
        '
        'PictureBox1
        '
        Me.PictureBox1.BackColor = System.Drawing.Color.Transparent
        Me.PictureBox1.BackgroundImage = Global.MULTINLINE_TEXTV_BOX.My.Resources.Resources.spook
        Me.PictureBox1.Enabled = False
        Me.PictureBox1.Location = New System.Drawing.Point(143, 61)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(512, 414)
        Me.PictureBox1.TabIndex = 1
        Me.PictureBox1.TabStop = False
        '
        'PictureBox2
        '
        Me.PictureBox2.BackColor = System.Drawing.SystemColors.MenuText
        Me.PictureBox2.Location = New System.Drawing.Point(1, 0)
        Me.PictureBox2.Name = "PictureBox2"
        Me.PictureBox2.Size = New System.Drawing.Size(534, 400)
        Me.PictureBox2.TabIndex = 6
        Me.PictureBox2.TabStop = False
        '
        'Spook
        '
        Me.Spook.BackColor = System.Drawing.Color.White
        Me.Spook.ForeColor = System.Drawing.Color.Fuchsia
        Me.Spook.Location = New System.Drawing.Point(293, 362)
        Me.Spook.Name = "Spook"
        Me.Spook.Size = New System.Drawing.Size(214, 23)
        Me.Spook.Style = System.Windows.Forms.ProgressBarStyle.Continuous
        Me.Spook.TabIndex = 12
        Me.Spook.Value = 50
        Me.Spook.Visible = False
        '
        'Bore
        '
        Me.Bore.BackColor = System.Drawing.Color.White
        Me.Bore.ForeColor = System.Drawing.Color.Green
        Me.Bore.Location = New System.Drawing.Point(293, 330)
        Me.Bore.Name = "Bore"
        Me.Bore.Size = New System.Drawing.Size(214, 23)
        Me.Bore.Style = System.Windows.Forms.ProgressBarStyle.Continuous
        Me.Bore.TabIndex = 13
        Me.Bore.Value = 50
        Me.Bore.Visible = False
        '
        'TextBox1
        '
        Me.TextBox1.BackColor = System.Drawing.Color.White
        Me.TextBox1.Enabled = False
        Me.TextBox1.Font = New System.Drawing.Font("Bookman Old Style", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBox1.Location = New System.Drawing.Point(190, 330)
        Me.TextBox1.Name = "TextBox1"
        Me.TextBox1.Size = New System.Drawing.Size(85, 22)
        Me.TextBox1.TabIndex = 14
        Me.TextBox1.Text = "Boreometer"
        Me.TextBox1.Visible = False
        '
        'TextBox2
        '
        Me.TextBox2.BackColor = System.Drawing.Color.Black
        Me.TextBox2.Enabled = False
        Me.TextBox2.Font = New System.Drawing.Font("Bookman Old Style", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBox2.ForeColor = System.Drawing.Color.White
        Me.TextBox2.Location = New System.Drawing.Point(190, 362)
        Me.TextBox2.Name = "TextBox2"
        Me.TextBox2.Size = New System.Drawing.Size(85, 22)
        Me.TextBox2.TabIndex = 15
        Me.TextBox2.Text = "Spookemeter"
        Me.TextBox2.Visible = False
        '
        'deathMsg
        '
        Me.deathMsg.Enabled = False
        Me.deathMsg.Font = New System.Drawing.Font("Comic Sans MS", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.deathMsg.Location = New System.Drawing.Point(190, 177)
        Me.deathMsg.Multiline = True
        Me.deathMsg.Name = "deathMsg"
        Me.deathMsg.Size = New System.Drawing.Size(332, 131)
        Me.deathMsg.TabIndex = 16
        Me.deathMsg.Visible = False
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.ControlDark
        Me.ClientSize = New System.Drawing.Size(534, 392)
        Me.Controls.Add(Me.deathMsg)
        Me.Controls.Add(Me.TextBox2)
        Me.Controls.Add(Me.TextBox1)
        Me.Controls.Add(Me.Bore)
        Me.Controls.Add(Me.Spook)
        Me.Controls.Add(Me.PictureBox3)
        Me.Controls.Add(Me.txtStory)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.Button2)
        Me.Controls.Add(Me.Button3)
        Me.Controls.Add(Me.PictureBox1)
        Me.Controls.Add(Me.Answer2)
        Me.Controls.Add(Me.Question)
        Me.Controls.Add(Me.Answer)
        Me.Controls.Add(Me.init)
        Me.Controls.Add(Me.Enter)
        Me.Controls.Add(Me.PictureBox2)
        Me.Name = "Form1"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Form1"
        CType(Me.PictureBox3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents txtStory As System.Windows.Forms.TextBox
    Friend WithEvents PictureBox1 As System.Windows.Forms.PictureBox
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents Button2 As System.Windows.Forms.Button
    Friend WithEvents Button3 As System.Windows.Forms.Button
    Friend WithEvents init As System.Windows.Forms.TextBox
    Friend WithEvents PictureBox2 As System.Windows.Forms.PictureBox
    Friend WithEvents Question As System.Windows.Forms.TextBox
    Friend WithEvents Answer As System.Windows.Forms.TextBox
    Friend WithEvents Enter As System.Windows.Forms.Button
    Friend WithEvents Answer2 As System.Windows.Forms.TextBox
    Friend WithEvents PictureBox3 As System.Windows.Forms.PictureBox
    Friend WithEvents Spook As System.Windows.Forms.ProgressBar
    Friend WithEvents Bore As System.Windows.Forms.ProgressBar
    Friend WithEvents TextBox1 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox2 As System.Windows.Forms.TextBox
    Friend WithEvents deathMsg As System.Windows.Forms.TextBox

End Class
